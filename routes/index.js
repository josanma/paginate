var express = require('express');
var router = express.Router();

const usuarioRoutes = require('./user.route');


router.use('/user', usuarioRoutes);

module.exports = router;
