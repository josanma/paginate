var express = require('express');
var router = express.Router();

const  { usuarioController } = require('../controllers');

router.get('/', usuarioController.getUsers);
module.exports = router;
