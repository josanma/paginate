const { usuario } = require('../daos');

const findAll = async ({skip, limit}) => {
    return await usuario.findAll({skip, limit});
}

module.exports = {
    findAll
}
