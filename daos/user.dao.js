const {usuarios = []} = require('../mocks');

const findAll = async ({skip = 0, limit = 10}) => {
    return {
        data: usuarios.slice(skip, skip + limit),
        total: usuarios.length
    };
}

module.exports = {
    findAll
}
