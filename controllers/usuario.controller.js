const url = require('url');
const {usuarioServices} = require('../services');

const getUsers = async (req, res) => {
    let {page = 1, limit = 10} = req.query;
    if (isNaN(page) || isNaN(limit)) {
        console.log('pico');
    } else {
        page = Number(page);
        limit = Number(limit);
    }

    const users = await usuarioServices.findAll({skip: (page - 1) * limit, limit});
    return res.json(
        {
            data: {
                prev: page !== 1 ? _fullUrl(req, {page: page - 1}) : undefined,
                next: (page * limit) < users.total ? _fullUrl(req, {page: page + 1}) : undefined,
                count: users.data.length,
                total: users.total,
                content: users.data
            }
        });
}

const _fullUrl = (req, query) => {
    return url.format({
        protocol: req.protocol,
        host: req.get('host'),
        pathname: req.baseUrl,
        query: {...req.query, ...query}
    });
}

module.exports = {
    getUsers
}
